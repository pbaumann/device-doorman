# Device Doorman

### Raspberry Pi
The Pi is easily setup to run continuous bluetooth MAC address searches with the following commands:

    sudo hciconfig hci0 up
    python ./btscan.py
    
The Kotlin server must be running first otherwise the requests will fail


### Kotlin Web Server and Browser UI
The Kotlin Server and simple Web UI need to be hosted on a publicly exposed machine. Currently we are using a droplet VM to host the server and also the webpage.


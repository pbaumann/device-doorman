import bluetooth
import requests
import json
import time

mac_statuses = {}

def post_func(add, status, name):
    print("inpostfunc: {}".format(add))
    url = "http://165.227.112.100:8080/person/" + add
    post_macs = requests.put(url, json = {"status":status, "name":name})
    print(post_macs.text)


c = 0
while(1):
    get_macs = requests.get("http://165.227.112.100:8080/person")
    get_macs = get_macs.json()
    print get_macs
    macs = []
    names = {}
    for i in get_macs:
        macs.append(i['macaddr'])
        names[i['macaddr']] = i['name']
    to_send = {}
    for mac in macs:
        print("MAC:{}".format(mac))
        if len(mac) != 17:
            print("MAC formatted wrong: {}".format(mac))
            continue
        res = bluetooth.lookup_name(mac, timeout = 5)
        if mac in mac_statuses.keys():
          if res != None and mac_statuses[mac] != "true":
            post_func(mac, 1, names[mac])
            mac_statuses[mac] = "true"
          elif res == None and mac_statuses[mac] == "true":
            post_func(mac, 0, names[mac])
            mac_statuses[mac] = "false"
        else:
          if res != None:
            post_func(mac, 1,names[mac])
            mac_statuses[mac] = "true"
          else:
            post_func(mac, 0,names[mac])
            mac_statuses[mac] = "false"
	print(mac_statuses[mac])
    if c==0:
        fh = open("/home/pi/device-doorman/ScanClient/report.txt", "w")
        fh.write("Looked up these MACs: {}".format(macs))
        fh.close
    c+=1
    time.sleep(1)
